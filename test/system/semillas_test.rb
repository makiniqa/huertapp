require "application_system_test_case"

class SemillasTest < ApplicationSystemTestCase
  setup do
    @semilla = semillas(:one)
  end

  test "visiting the index" do
    visit semillas_url
    assert_selector "h1", text: "Semillas"
  end

  test "creating a Semilla" do
    visit semillas_url
    click_on "New Semilla"

    fill_in "Cantidad", with: @semilla.cantidad
    fill_in "Fecha siembra", with: @semilla.fecha_siembra
    fill_in "Nombre", with: @semilla.nombre
    fill_in "Origen", with: @semilla.origen
    fill_in "Tipo", with: @semilla.tipo
    click_on "Create Semilla"

    assert_text "Semilla was successfully created"
    click_on "Back"
  end

  test "updating a Semilla" do
    visit semillas_url
    click_on "Edit", match: :first

    fill_in "Cantidad", with: @semilla.cantidad
    fill_in "Fecha siembra", with: @semilla.fecha_siembra
    fill_in "Nombre", with: @semilla.nombre
    fill_in "Origen", with: @semilla.origen
    fill_in "Tipo", with: @semilla.tipo
    click_on "Update Semilla"

    assert_text "Semilla was successfully updated"
    click_on "Back"
  end

  test "destroying a Semilla" do
    visit semillas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Semilla was successfully destroyed"
  end
end
