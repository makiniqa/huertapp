require "application_system_test_case"

class SemillasTest < ApplicationSystemTestCase
  setup do
    @semillas = semillass(:one)
  end

  test "visiting the index" do
    visit semillass_url
    assert_selector "h1", text: "Semillas"
  end

  test "creating a Semillas" do
    visit semillass_url
    click_on "New Semillas"

    fill_in "Cantidad", with: @semillas.cantidad
    fill_in "Fecha siembra", with: @semillas.fecha_siembra
    fill_in "Nombre", with: @semillas.nombre
    fill_in "Origen", with: @semillas.origen
    fill_in "Tipo", with: @semillas.tipo
    click_on "Create Semillas"

    assert_text "Semillas was successfully created"
    click_on "Back"
  end

  test "updating a Semillas" do
    visit semillass_url
    click_on "Edit", match: :first

    fill_in "Cantidad", with: @semillas.cantidad
    fill_in "Fecha siembra", with: @semillas.fecha_siembra
    fill_in "Nombre", with: @semillas.nombre
    fill_in "Origen", with: @semillas.origen
    fill_in "Tipo", with: @semillas.tipo
    click_on "Update Semillas"

    assert_text "Semillas was successfully updated"
    click_on "Back"
  end

  test "destroying a Semillas" do
    visit semillass_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Semillas was successfully destroyed"
  end
end
