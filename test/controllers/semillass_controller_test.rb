require 'test_helper'

class SemillassControllerTest < ActionDispatch::IntegrationTest
  setup do
    @semillas = semillass(:one)
  end

  test "should get index" do
    get semillass_url
    assert_response :success
  end

  test "should get new" do
    get new_semillas_url
    assert_response :success
  end

  test "should create semillas" do
    assert_difference('Semillas.count') do
      post semillass_url, params: { semillas: { cantidad: @semillas.cantidad, fecha_siembra: @semillas.fecha_siembra, nombre: @semillas.nombre, origen: @semillas.origen, tipo: @semillas.tipo } }
    end

    assert_redirected_to semillas_url(Semillas.last)
  end

  test "should show semillas" do
    get semillas_url(@semillas)
    assert_response :success
  end

  test "should get edit" do
    get edit_semillas_url(@semillas)
    assert_response :success
  end

  test "should update semillas" do
    patch semillas_url(@semillas), params: { semillas: { cantidad: @semillas.cantidad, fecha_siembra: @semillas.fecha_siembra, nombre: @semillas.nombre, origen: @semillas.origen, tipo: @semillas.tipo } }
    assert_redirected_to semillas_url(@semillas)
  end

  test "should destroy semillas" do
    assert_difference('Semillas.count', -1) do
      delete semillas_url(@semillas)
    end

    assert_redirected_to semillass_url
  end
end
