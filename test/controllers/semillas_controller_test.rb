require 'test_helper'

class SemillasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @semilla = semillas(:one)
  end

  test "should get index" do
    get semillas_url
    assert_response :success
  end

  test "should get new" do
    get new_semilla_url
    assert_response :success
  end

  test "should create semilla" do
    assert_difference('Semilla.count') do
      post semillas_url, params: { semilla: { cantidad: @semilla.cantidad, fecha_siembra: @semilla.fecha_siembra, nombre: @semilla.nombre, origen: @semilla.origen, tipo: @semilla.tipo } }
    end

    assert_redirected_to semilla_url(Semilla.last)
  end

  test "should show semilla" do
    get semilla_url(@semilla)
    assert_response :success
  end

  test "should get edit" do
    get edit_semilla_url(@semilla)
    assert_response :success
  end

  test "should update semilla" do
    patch semilla_url(@semilla), params: { semilla: { cantidad: @semilla.cantidad, fecha_siembra: @semilla.fecha_siembra, nombre: @semilla.nombre, origen: @semilla.origen, tipo: @semilla.tipo } }
    assert_redirected_to semilla_url(@semilla)
  end

  test "should destroy semilla" do
    assert_difference('Semilla.count', -1) do
      delete semilla_url(@semilla)
    end

    assert_redirected_to semillas_url
  end
end
