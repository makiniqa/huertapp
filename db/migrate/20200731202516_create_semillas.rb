class CreateSemillas < ActiveRecord::Migration[6.0]
  def change
    create_table :semillas do |t|
      t.string :nombre
      t.string :tipo
      t.string :origen
      t.float :cantidad
      t.date :fecha_siembra

      t.timestamps
    end
  end
end
