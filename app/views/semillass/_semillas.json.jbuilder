json.extract! semillas, :id, :nombre, :tipo, :origen, :cantidad, :fecha_siembra, :created_at, :updated_at
json.url semillas_url(semillas, format: :json)
