json.extract! semilla, :id, :nombre, :tipo, :origen, :cantidad, :fecha_siembra, :created_at, :updated_at
json.url semilla_url(semilla, format: :json)
